import time
from datetime import datetime, time, date, timedelta
from flask import current_app
from flask_apscheduler import APScheduler

from . models import db, Visitor, StaffLog

scheduler = APScheduler()
scheduler.api_enabled = False

@scheduler.task('cron', id='yo', second='*/5')
def yo():
    print("yo")
    with scheduler.app.app_context():

        current_app.logger.info('Signing out yesterday')

@scheduler.task('cron', id='sign_out_yesterday', hour='1')
def sign_out_yesterday():
    """Sign out yesterday's visitors and staff."""
    yesterday = datetime.combine(date.today() - timedelta(days=1), time.max)

    with scheduler.app.app_context():

        current_app.logger.info('Signing out yesterday')

        StaffLog.query.filter(StaffLog.date_time_in <= yesterday) \
            .filter(StaffLog.date_time_out == None) \
            .update({StaffLog.date_time_out: yesterday})

        Visitor.query.filter(Visitor.date_time_in <= yesterday) \
            .filter(Visitor.date_time_out == None) \
            .update({Visitor.date_time_out: yesterday})

        db.session.commit()