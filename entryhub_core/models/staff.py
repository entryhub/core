import datetime
import random
from flask import url_for, current_app
from ..exceptions import ValidationError
from .base import db
from .stafflog import StaffLog


class Staff(db.Model):
    """Staff Class."""

    __tablename__ = 'staff'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(64))
    first_name = db.Column(db.String(64))
    last_name = db.Column(db.String(64))
    date_of_birth = db.Column(db.Date)
    group = db.Column(db.String(64), default='Staff')
    active = db.Column(db.Boolean(), default=True)
    notes = db.Column(db.Text())
    logs = db.relationship('StaffLog', backref='staff', lazy='dynamic')

    def sign_in(self):
        """Sign the staff in."""
        if self.status == 'out':
            member_log = StaffLog(staff_id=self.id)
            db.session.add(member_log)
            db.session.commit()
            return True
        else:
            return False

    def sign_out(self):
        """Sign the staff out."""
        if self.status == 'in':
            member_log = self.logs.filter(
                StaffLog.staff_id == self.id, StaffLog.date_time_out == None).first()
            member_log.date_time_out = datetime.datetime.now()
            db.session.add(member_log)
            db.session.commit()
            return True
        else:
            return False

    @property
    def status(self):
        """Return in or out if the staff is signed in or out."""
        if (self.logs.filter(StaffLog.staff_id == self.id, StaffLog.date_time_out == None).first() is None):
            return 'out'
        else:
            return 'in'

    @staticmethod
    def generate_fake(amount=100):
        """Generator an ammount of fake staff for testing."""
        from sqlalchemy.exc import IntegrityError
        from random import seed
        import requests

        service_url = "http://api.randomuser.me/?nat=gb&results={amount}"
        request = requests.get(service_url.format(amount=amount))

        if not request.status_code == 200:
            return False

        data = request.json()
        total_results = len(data['results'])
        count = 0

        seed()

        #print(total_results)

        #yield {'current': 0, 'total': total_results}

        for member in data['results']:
            count += 1

            s = Staff(
                title=member['name']['title'].capitalize(),
                first_name=member['name']['first'].capitalize(),
                last_name=member['name']['last'].capitalize(),
                date_of_birth=datetime.datetime.strptime(member['dob']['date'], '%Y-%m-%dT%H:%M:%S.%fZ').date(),
                group=random.choice([
                    'Staff',
                    'Staff',
                    'Staff',
                    'Staff',
                    'Staff',
                    'Staff',
                    'Governors',
                    'Supply Agency'
                ]),
                active=True
            )
            db.session.add(s)
            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

            one_day = datetime.timedelta(days=1)
            date = datetime.datetime.now().date()
            total_days = 7 * 26

            current_app.logger.info("generating member log %s of %s" % (count, total_results))

            for n in range(total_days):
                if date.weekday() != 7 and date.weekday() != 6:
                    start_time = datetime.datetime.strptime(
                        "%s 09:%02d:%02d" % (date, random.randint(0, 10), random.randint(0, 59)),
                        "%Y-%m-%d %H:%M:%S")

                    dinner_hour = random.choice([12, 13])
                    dinner_time = datetime.datetime.strptime(
                        "%s %02d:%02d:%02d" % (date, dinner_hour, random.randint(0, 10), random.randint(0, 59)),
                        "%Y-%m-%d %H:%M:%S")

                    start_back_time = datetime.datetime.strptime(
                        "%s %02d:%02d:%02d" % (date, dinner_hour + 1, random.randint(0, 10), random.randint(0, 59)),
                        "%Y-%m-%d %H:%M:%S")

                    end_time = datetime.datetime.strptime(
                        "%s 17:%02d:%02d" % (date, random.randint(0, 10), random.randint(0, 59)),
                        "%Y-%m-%d %H:%M:%S")

                    db.session.add(StaffLog(
                        staff_id=s.id,
                        date_time_in=start_time,
                        date_time_out=dinner_time))

                    db.session.add(StaffLog(
                        staff_id=s.id,
                        date_time_in=start_back_time,
                        date_time_out=end_time))

                date -= one_day

            current_app.logger.info("commiting staff log %s of %s" % (count, total_results))

            try:
                db.session.commit()
            except IntegrityError:
                current_app.logger.info("commiting staff log error %s of %s" % (count, total_results))
                db.session.rollback()

            #yield {'current': count, 'total': total_results}

    def to_dict(self):
        """Return a dict of the staff class."""

        return {
            'id': self.id,
            'title': self.title,
            'firstName': self.first_name,
            'lastName': self.last_name,
            'dateOfBirth': self.date_of_birth.strftime('%Y-%m-%d'),
            'group': self.group,
            'active': self.active,
            'notes': self.notes,
            'status': self.status,
        }

    @staticmethod
    def from_json(json_post):
        """Create new member from JSON."""
        title = json_post.get('title')
        first_name = json_post.get('firstName')
        last_name = json_post.get('lastName')
        date_of_birth = json_post.get('dateOfBirth')
        group = json_post.get('group')
        active = json_post.get('active')

        if title is None or title == '':
            raise ValidationError('member does not have a title')

        if first_name is None or first_name == '':
            raise ValidationError('member does not have a firstName')

        if last_name is None or last_name == '':
            raise ValidationError('member does not have a lastName')

        if date_of_birth is None or date_of_birth == '':
            raise ValidationError('member does not have a dateOfBirth')

        if active is None or active == '':
            active = True
        elif str(active).lower() == 'true':
            active = True
        elif str(active).lower() == 'false':
            active = False
        else:
            raise ValidationError('active is only true, false or none')

        if group is None or group == '':
            group = 'Staff'

        member = Staff()
        member.update_from_json(json_post)
        return member

    def update_from_json(self, json_post):
        """Update current member from JSON."""
        title = json_post.get('title')
        first_name = json_post.get('firstName')
        last_name = json_post.get('lastName')
        date_of_birth = json_post.get('dateOfBirth')
        group = json_post.get('group')
        active = json_post.get('active')

        if title is not None and title != '':
            self.title = title

        if first_name is not None and first_name != '':
            self.first_name = first_name

        if last_name is not None and last_name != '':
            self.last_name = last_name

        if date_of_birth is not None and date_of_birth != '':
            try:
                self.date_of_birth = datetime.datetime.strptime(date_of_birth, '%Y-%m-%d').date()
            except ValueError:
                raise ValidationError('incorrect dateOfBirth format, should be YYYY-MM-DD')

        if str(active).lower() == 'true':
            self.active = True
        elif str(active).lower() == 'false':
            self.active = False
        elif active is not None and active != '':
            raise ValidationError('active is only true, false or none')

        if group is not None and group != '':
            self.group = group

        db.session.add(self)
        db.session.commit()

    def __repr__(self):
        return '<Staff %r %r>' % (self.first_name, self.last_name)
