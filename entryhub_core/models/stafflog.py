
import datetime
from .base import db


class StaffLog(db.Model):
    """Staff Log Class."""

    __tablename__ = 'staff_logs'
    id = db.Column(db.Integer, primary_key=True)
    staff_id = db.Column(db.Integer, db.ForeignKey('staff.id'))
    date_time_in = db.Column(db.DateTime, default=datetime.datetime.now)
    date_time_out = db.Column(db.DateTime, default=None)

    @property
    def total(self):
        """Return a total amount of time passed between date_time_in and date_time_out."""
        if (self.date_time_out is None):
            return datetime.datetime.now() - self.date_time_in
        else:
            return self.date_time_out - self.date_time_in

    def to_dict(self):
        """Return class in a dict format."""
        return {'id': self.id,
                'dateTimeIn': self.date_time_in,
                'dateTimeOut': self.date_time_out,
                'total': str(self.total)}

    def __repr__(self):
        return '<StaffLog %r %r>' % (self.id, self.staff_id)
