from .base import db


class Setting(db.Model):
    """Pupil Sign in"""

    __tablename__ = 'settings'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    value = db.Column(db.PickleType(mutable=True), nullable=False)
    last_modified = db.Column(db.DateTime)

    def to_dict(self) -> dict:
        """Return class in a dict format."""

        return {
            'id': self.id,
            'name': self.name,
            'value': self.value,
            }