
import datetime
import random
import io
import string
from flask import url_for
from .. exceptions import ValidationError
from . base import db


class Visitor(db.Model):
    """Visitor Class."""

    __tablename__ = 'visitors'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64))
    company = db.Column(db.String(64))
    car_registration = db.Column(db.String(64))
    reason = db.Column(db.Text)
    date_time_in = db.Column(db.DateTime, default=datetime.datetime.now)
    date_time_out = db.Column(db.DateTime, default=None)
    badge_token = db.Column(db.String(5), index=True, unique=True)

    def __init__(self, **kwargs):
        """Generate badge token when new class is created if one hasn't been defined."""
        super(Visitor, self).__init__(**kwargs)
        if self.badge_token is None and self.date_time_out is None:
            self.__generate_badge_token()

    @property
    def status(self):
        """Return if the member is signed in or out."""
        if self.date_time_out is None:
            return 'in'
        else:
            return 'out'

    @staticmethod
    def generate_fake(days=365):
        """Generate fake visitors for an amount of days.

        Will generate between 5 and 15 visitors for each day.
        """
        from sqlalchemy.exc import IntegrityError
        from random import seed
        from faker import Faker

        fake = Faker()

        one_day = datetime.timedelta(days=1)
        date = datetime.datetime.now().date()
        today = date

        for n in range(days):
            if date.weekday() != 7 and date.weekday() != 6:
                seed()
                amount_visitors = random.randint(5, 15)

                for n in range(amount_visitors):
                    seed()
                    time_in_hour = random.randint(9, 15)
                    time_in = datetime.datetime.strptime(
                        "%s %02d:%02d:%02d" % (
                            date,
                            time_in_hour,
                            random.randint(0, 10),
                            random.randint(0, 59)),
                        "%Y-%m-%d %H:%M:%S")

                    if (date == today) and (0 > random.randint(0, 3)):
                        time_out = None
                    else:
                        if (time_in_hour <= 13):
                            time_out_max_hour = time_in_hour + 4

                        elif (time_in_hour > 13):
                            time_out_max_hour = time_in_hour + 3

                        elif (time_in_hour > 14):
                            time_out_max_hour = time_in_hour + 2

                        elif (time_in_hour > 15):
                            time_out_max_hour = time_in_hour + 1

                        time_out_hour = random.randint(
                                time_in_hour + 1,
                                time_out_max_hour)

                        time_out = datetime.datetime.strptime(
                            "%s %02d:%02d:%02d" % (
                                date,
                                time_out_hour,
                                random.randint(0, 10),
                                random.randint(0, 59)),
                            "%Y-%m-%d %H:%M:%S")

                    v = Visitor(name=fake.name(),
                                company=fake.company(),
                                car_registration=fake.license_plate(),
                                reason=fake.paragraph(
                                    nb_sentences=1, variable_nb_sentences=True, ext_word_list=None),
                                date_time_in=time_in,
                                date_time_out=time_out)
                    db.session.add(v)

            date -= one_day

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    def __generate_badge_token(self):
        """Generate unique badge token."""
        chars = string.ascii_uppercase + string.digits
        chars = chars.replace("V", "").replace("M", "")

        used = 1
        while used is not None:
            token = ''
            for num in range(5):
                token = "%s%s" % (token, random.choice(chars))

            token = "V%s" % token
            used = Visitor.query.filter_by(badge_token=token).first()

        self.badge_token = token

    def sign_out(self):
        """Sign out visitor."""
        if self.status == 'in':
            self.date_time_out = datetime.datetime.now()
            self.badge_token = None
            db.session.add(self)
            db.session.commit()
            return True
        else:
            return False

    def to_dict(self) -> dict:
        """Return class in a dict format."""
        if self.date_time_out == None:
            date_time_out = None
        else:
            date_time_out = self.date_time_out.isoformat('T')

        return {
            'id': self.id,
            'name': self.name,
            'carRegistration': self.car_registration,
            'company': self.company,
            'reason': self.reason,
            'dateTimeIn': self.date_time_in.isoformat('T'),
            'dateTimeOut': date_time_out,
            'status': self.status
            }

    @staticmethod
    def from_json(json_post):
        """Return new class from JSON."""
        name = json_post.get('name')
        company = json_post.get('company')
        reason = json_post.get('reason')
        car_registration = json_post.get('carRegistration')

        if name is None:
            raise ValidationError('visitor does not have name')

        return Visitor(
            name=name,
            company=company,
            reason=reason,
            car_registration=car_registration
        )

    def __repr__(self):
        return '<Visitor %r>' % self.name
