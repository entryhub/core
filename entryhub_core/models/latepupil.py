
import datetime

from .base import db


class LatePupil(db.Model):
    """Pupil Sign in"""

    __tablename__ = 'late_pupils'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String)
    class_name = db.Column(db.String)
    reason = db.Column(db.String)
    date_time_in = db.Column(db.DateTime, default=datetime.datetime.now)

    def to_dict(self) -> dict:
        """Return class in a dict format."""

        return {
            'id': self.id,
            'name': self.name,
            'className': self.class_name,
            'reason': self.reason,
            'dateTimeIn': self.date_time_in.isoformat('T')
            }