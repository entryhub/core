import datetime

from . base import jsonrpc_1_0
from .. models import StaffLog, Visitor, LatePupil

@jsonrpc_1_0.method('system.health')
def system_ping() -> dict:
    return {
        "status": "ok",
        "dateTime": datetime.datetime.now().isoformat('T')
    }


@jsonrpc_1_0.method('system.whosin')
def whos_in_list() -> dict:
    day = datetime.date.today()
    next_day = day + datetime.timedelta(days=1)
    staff_log = StaffLog.query.filter(StaffLog.date_time_out == None)
    visitors = Visitor.query.order_by(Visitor.name).filter(Visitor.date_time_out == None)
    late_pupils = LatePupil.query.order_by(LatePupil.name) \
                    .filter(LatePupil.date_time_in >= day, LatePupil.date_time_in < next_day)
    return {
        "staff": [{
            "name": f"{staff.staff.first_name} {staff.staff.last_name}",
            "group": staff.staff.group
            }
            for staff in staff_log],
        "visitors": [{"name": visitor.name, "company": visitor.company} for visitor in visitors],
        "latePupils": [{"name": pupil.name, "className": pupil.class_name} for pupil in late_pupils]
    }
