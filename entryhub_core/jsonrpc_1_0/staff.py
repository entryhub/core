import datetime
from sqlalchemy.sql.expression import Extract

from . base import jsonrpc_1_0
from .. models import db, Staff, StaffLog


@jsonrpc_1_0.method('staff.list')
def staff_list(dayOfBirth:int=0, active:bool=True, search:str or None=None, limit:int=1000) -> list:
    staff = Staff.query.filter(Staff.active == active)

    if dayOfBirth > 0:
        if dayOfBirth <= 31:
            staff = staff.filter(Extract('day', Staff.date_of_birth) == dayOfBirth)
        else:
            raise ValueError('Invalid day of birth')

    if search:
        staff = staff.filter(
            Staff.last_name.ilike(f"%{search}%") |
            Staff.first_name.ilike(f"%{search}%") |
            Staff.group.ilike(f"%{search}%")
        )

    staff = staff.order_by(Staff.last_name).limit(limit)

    return [s.to_dict() for s in staff]

@jsonrpc_1_0.method('staff.create')
def staff_create() -> str:
    return "OK"

@jsonrpc_1_0.method('staff.sign_in_out')
def staff_sign_in_out(id: int) -> str:
    s = Staff.query.get_or_404(id)
    if s.status == "out":
        s.sign_in()
    else:
        s.sign_out()
    return "OK"

@jsonrpc_1_0.method('staff.details')
def staff_details(id: int) -> dict:
    #TODO: ERROR Handling
    m = Staff.query.get_or_404(id)

    return m.to_dict()

@jsonrpc_1_0.method('staff.logs')
def staff_logs(id: int, dateFrom:str or None=None, dateTo:str or None=None) -> dict:

    Staff.query.get_or_404(id)
    formated_logs = []
    logs = {}

    ml = StaffLog.query.filter(StaffLog.staff_id == id)

    if dateFrom is not None or dateTo is not None:
        if dateFrom is not None and dateTo is not None:
            ml = ml.filter(StaffLog.date_time_in >= datetime.datetime.fromisoformat(f"{dateFrom}T00:00:00"))
            ml = ml.filter(StaffLog.date_time_in<= datetime.datetime.fromisoformat(f"{dateTo}T23:59:59"))
        else:
            raise ValueError("Both dateFrom and dateTo are required")

    ml = ml.order_by(StaffLog.date_time_in.desc())

    # TODO: A Better Way
    for log in ml.all():
        date = log.date_time_in.date()
        if log.date_time_out is None:
            total = datetime.timedelta()
        else:
            total = log.date_time_out - log.date_time_in

        week_begining = str(date - datetime.timedelta(days=date.weekday()))
        date = str(date)

        if week_begining not in logs:
            logs[week_begining] = {}
            logs[week_begining]['dates'] = {}
            logs[week_begining]['total'] = datetime.timedelta()

        if date in logs[week_begining]['dates']:
            logs[week_begining]['dates'][date]['total'] += total

        else:
            logs[week_begining]['dates'][date] = {}
            logs[week_begining]['dates'][date]['times'] = []
            logs[week_begining]['dates'][date]['total'] = total

        if log.date_time_out != None:
            if str(log.date_time_out.date()) != date:
                time_out = str(log.date_time_out)
            else:
                time_out = str(log.date_time_out.time())
        else:
            time_out = ''

        logs[week_begining]['dates'][date]['times'].append([
            str(log.date_time_in.time()),
            time_out,
            str(total)])


        logs[week_begining]['total'] += total


    for week, week_data in logs.items():

        dates = []
        for date, date_data in week_data['dates'].items():

            times = []
            for time in date_data['times']:
                times.append({
                    'timeIn': time[0],
                    'timeOut': time[1],
                    'total': time[2]
                })

            dates.append({
                'date': date,
                'total': str(date_data['total']),
                'times': times
            })

        hours, remainder = divmod(week_data['total'].total_seconds(), 3600)
        minutes, seconds = divmod(remainder, 60)

        formated_logs.append({
            'weekBegining': week,
            'total': '%02d:%02d:%02d' % (hours, minutes, seconds),
            'dates': dates
        })

    return {
        'staffId': id,
        'logs': formated_logs
    }