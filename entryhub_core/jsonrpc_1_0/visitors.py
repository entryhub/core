from datetime import datetime

from . base import jsonrpc_1_0
from .. models import db,  Visitor

@jsonrpc_1_0.method('visitors.list')
def visitors_list(status:str="all", limit:int=1000, dateFrom:str or None=None, dateTo:str or None=None, search: str or None=None, page: int=1) -> list:
    visitors = Visitor.query

    match status:
        case "all":
            pass

        case "in":
            visitors = visitors.filter(Visitor.date_time_out == None)

        case "out":
            visitors = visitors.filter(Visitor.date_time_out != None)

        case __:
            raise ValueError("Invalid status")

    # TODO: Better Validation
    if dateFrom is not None or dateTo is not None:
        if dateFrom is not None and dateTo is not None:
            visitors = visitors.filter(Visitor.date_time_in >= datetime.fromisoformat(f"{dateFrom}T00:00:00"))
            visitors = visitors.filter(Visitor.date_time_in <= datetime.fromisoformat(f"{dateTo}T23:59:59"))
        else:
            raise ValueError("Both dateFrom and dateTo are required")

    if search is not None:
        visitors = visitors.filter(
            Visitor.name.ilike(f"%{search}%") |
            Visitor.company.ilike(f"%{search}%") |
            Visitor.reason.ilike(f"%{search}%")
        )

    visitors = visitors.order_by(Visitor.date_time_in.desc()).limit(limit)

    return [visitor.to_dict() for visitor in visitors]

@jsonrpc_1_0.method('visitors.create')
def visitors_create(
    name: str,
    company: str,
    reason: str,
    carRegistration: str
) -> str:
    #TODO: ERROR Handling
    v = Visitor(
        name=name,
        company=company,
        reason=reason,
        car_registration=carRegistration
    )

    db.session.add(v)
    db.session.commit()

    return "OK"

@jsonrpc_1_0.method('visitors.sign_out')
def visitors_signout(id: int) -> str:
    #TODO: ERROR Handling
    v = Visitor.query.get(id)
    v.sign_out()

    return "OK"