from datetime import datetime

from . base import jsonrpc_1_0
from .. models import db,  LatePupil

@jsonrpc_1_0.method('latepupils.list')
def late_pupils_list(limit:int=1000, dateFrom:str or None=None, dateTo:str or None=None, search: str or None=None, page: int=1) -> list:
    pupils = LatePupil.query

    # TODO: Better Validation
    if dateFrom is not None or dateTo is not None:
        if dateFrom is not None and dateTo is not None:
            pupils = pupils.filter(LatePupil.date_time_in >= datetime.fromisoformat(f"{dateFrom}T00:00:00"))
            pupils = pupils.filter(LatePupil.date_time_in <= datetime.fromisoformat(f"{dateTo}T23:59:59"))
        else:
            raise ValueError("Both dateFrom and dateTo are required")

    if search is not None:
        pupils = pupils.filter(
            LatePupil.name.ilike(f"%{search}%") |
            LatePupil.class_name.ilike(f"%{search}%") |
            LatePupil.reason.ilike(f"%{search}%")
        )

    pupils = pupils.order_by(LatePupil.date_time_in.desc()).limit(limit)
    return [pupil.to_dict() for pupil in pupils]


@jsonrpc_1_0.method('latepupils.create')
def late_pupils_create(
    name: str,
    className: str,
    reason: str
) -> str:
    #TODO: ERROR Handling
    p = LatePupil(
        name=name,
        class_name=className,
        reason=reason
    )

    db.session.add(p)
    db.session.commit()

    return "OK"