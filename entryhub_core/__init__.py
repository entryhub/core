
import logging
import os
import click
from flask import Flask, send_from_directory
from flask.cli import FlaskGroup
from flask_migrate import Migrate
from flask_cors import CORS
from logging.config import dictConfig
from . default_config import default_config
from . cli import register_cli_commands
from . scheduler import scheduler
from . models import db
from .jsonrpc_1_0 import jsonrpc_1_0


def create_app(config_name=os.getenv('FLASK_ENV') or 'default', load_blueprint=True , start_scheduler=True):

    def is_debug_mode():
        """Get app debug status."""
        debug = os.environ.get("FLASK_DEBUG")
        if not debug:
            return os.environ.get("FLASK_ENV") == "development"
        return debug.lower() not in ("0", "false", "no")

    def is_werkzeug_reloader_process():
        """Get werkzeug status."""
        return os.environ.get("WERKZEUG_RUN_MAIN") == "true"

    app = Flask(__name__,  static_url_path='', static_folder='frontend')
    app.config.from_object(default_config[config_name])
    default_config[config_name].init_app(app)

    migrate = Migrate(app, db, os.path.abspath(os.path.join(os.path.dirname(__file__), "migrations")))
    db.init_app(app)
    jsonrpc_1_0.init_app(app)
    CORS(app, resources={r"/api/*": {"origins": "*"}})
    scheduler.init_app(app)

    logging.getLogger("apscheduler").setLevel(logging.INFO)

    with app.app_context():

        if (is_debug_mode() and is_werkzeug_reloader_process()):
            app.logger.info("Starting scheduler")
            scheduler.start()

    register_cli_commands(app, create_app)


    # Serve React App
    @app.route('/manage', defaults={'path': ''})
    @app.route('/manage/<path:path>')
    def serve(path):
        if path != "" and os.path.exists(os.path.join(app.static_folder, 'manage', path)):
            return send_from_directory(os.path.join(app.static_folder, 'manage'), path)
        else:
            return send_from_directory(os.path.join(app.static_folder, 'manage'), 'index.html')

    if load_blueprint:
        pass

    return app


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    """Management script for the EntryHub."""
