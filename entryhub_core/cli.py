from pydoc import cli
import click
import os
from flask.cli import AppGroup
from . models import db, Staff, Visitor, StaffLog, LatePupil
from . scheduler import scheduler

def register_cli_commands(app, create_app):
    """
    Register all the cli commands for entryhub.
    """

    # Create a group for the entryhub mock commands
    mock_cli = AppGroup('mock')

    @mock_cli.command('staff')
    def mock_staff():
        #TODO: Move out of model class
        Staff.generate_fake()

    @mock_cli.command('visitors')
    def mock_visitors():
        #TODO: Move out of model class
        Visitor.generate_fake()

    app.cli.add_command(mock_cli)

    import_cli = AppGroup('import')

    @import_cli.command('staff')
    @click.argument('file')
    def import_staff(file):
        import json
        import datetime
        from sqlalchemy.exc import IntegrityError

        with open(file) as f:
            data = json.load(f)

        for member in data:
            print(f"* importing {member['firstName']} {member['lastName']}")
            s = Staff(
                title=member['title'],
                first_name=member['firstName'],
                last_name=member['lastName'],
                date_of_birth=datetime.datetime.strptime(member['dateOfBirth'], '%Y-%m-%d').date(),
                group=member['group'],
                notes=member['notes'],
                active=bool(member['active'])
            )
            db.session.add(s)

            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()

            print(f"    * importing logs")

            for log in member['logs']:
                try:
                    time_out = datetime.datetime.strptime(log['timeOut'], '%Y-%m-%d %H:%M:%S')
                except:
                    time_out = None

                db.session.add(
                    StaffLog(
                        staff_id=s.id,
                        date_time_in=datetime.datetime.strptime(log['timeIn'], '%Y-%m-%d %H:%M:%S'),
                        date_time_out=time_out
                        )
                )

            try:
                db.session.commit()
            except IntegrityError:
                db.session.rollback()


    @import_cli.command('visitors')
    @click.argument('file')
    def import_visitors(file):
        import json
        import datetime
        from sqlalchemy.exc import IntegrityError

        with open(file) as f:
            data = json.load(f)

        for visitor in data:
            print(f"* importing {visitor['name']}")

            try:
                time_out = datetime.datetime.strptime(visitor['timeOut'], '%Y-%m-%d %H:%M:%S')
            except:
                time_out = None

            v = Visitor(
                name=visitor['name'],
                company=visitor['company'],
                reason=visitor['reason'],
                car_registration=visitor['carRegistration'],
                date_time_in=datetime.datetime.strptime(visitor['timeIn'], '%Y-%m-%d %H:%M:%S'),
                date_time_out=time_out
            )

            db.session.add(v)

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()

    @import_cli.command('late_pupils')
    @click.argument('file')
    def import_late_pupils(file):
        import json
        import datetime
        from sqlalchemy.exc import IntegrityError

        with open(file) as f:
            data = json.load(f)

        for pupil in data:
            print(f"* importing {pupil['name']}")

            p = LatePupil(
                name=pupil['name'],
                class_name=pupil['className'],
                reason=pupil['reason'],
                date_time_in=datetime.datetime.strptime(pupil['timeIn'], '%Y-%m-%d %H:%M:%S'),
            )

            db.session.add(p)

        try:
            db.session.commit()
        except IntegrityError:
            db.session.rollback()


    app.cli.add_command(import_cli)

    @app.cli.command("serve")
    def serve_app():
        from waitress import serve
        import logging
        logger = logging.getLogger('waitress')
        logger.setLevel(logging.INFO)

        app = create_app()

        with app.app_context():
            app.logger.info("Starting scheduler")
            scheduler.start()

        serve(app, listen='*:8080', ident="EntryHub")